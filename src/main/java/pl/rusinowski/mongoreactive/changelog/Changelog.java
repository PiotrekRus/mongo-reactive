package pl.rusinowski.mongoreactive.changelog;

import com.github.cloudyrock.mongock.ChangeLog;
import com.github.cloudyrock.mongock.ChangeSet;
import com.github.cloudyrock.mongock.driver.mongodb.springdata.v3.decorator.impl.MongockTemplate;
import org.springframework.data.mongodb.core.CollectionOptions;
import pl.rusinowski.mongoreactive.Event;

@ChangeLog
public class Changelog {

    @ChangeSet(order = "001", id = "create collection", author = "rusinowski")
    public void createCollection(MongockTemplate mongockTemplate) {
        final var collectionOptions = new CollectionOptions(500L, 10000L, true);
        mongockTemplate.createCollection(Event.class, collectionOptions);


    }

    @ChangeSet(order = "002", id = "test data", author = "rusinowski")
    public void addTestRecords(MongockTemplate mongockTemplate) {
        mongockTemplate.save(Event.builder().eventName("test1").build());
        mongockTemplate.save(Event.builder().eventName("test2").build());
    }
}
